#Week 4 Creative Coding Web

www.packtpub.com/all?search=&offset=12&rows=&sort=

For **PacktPub Layout** images are 150 x  185. Use placehold it to create images:

<img src="http://placehold.it/150x185">


www.paulsmith.com/uk/mens/shoes/boots

For **Paul Smith** images are 360 x 360

<img src="http://placehold.it/360x360?text=Paul+Smith">


http://www.foyles.co.uk/computing-it

For **Foyles** images are 100 x 150

<img src="http://placehold.it/100x150/0000ff/ffffff?text=Foyles">

ReMaking History, Volume 3
By William Gurstelle
February 2017 (est.)
Pre-Order  Print :  $19.99